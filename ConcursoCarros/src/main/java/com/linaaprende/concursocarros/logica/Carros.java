/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linaaprende.concursocarros.logica;

import com.linaaprende.concursocarros.interfaz.Pista;

/**
 *
 * @author linaz
 */
public class Carros {
    private int cRojo;
    private int cNegro;
    private int cAmarillo;
    private int cVerde;
    
    public Carros(){   
    }
    
    public int getCRojo() {
        return cRojo;
    }

    public void setCRojo(int cRojo) {
        this.cRojo = cRojo;
    }
    public int getCNegro() {
        return cNegro;
    }

    public void setCNegro(int cNegro) {
        this.cNegro = cNegro;
    }
   
    public int getCAmarillo() {
        return cAmarillo;
    }

    public void setCAmarillo(int cAmarillo) {
        this.cAmarillo = cAmarillo;
    }
    public int getCVerde() {
        return cVerde;
    }

    public void setCVerde(int cVerde) {
        this.cVerde = cVerde;
    }
    
    public void avanzacarros(Pista pista, Juego juego,int turno){
        
            switch(turno){
                case 1: 
                    setCRojo(pista.avanzar(getCRojo(), juego));
                    break;
                case 2:
                    setCNegro(pista.avanzar(getCNegro(), juego));
                    break;
                case 3:
                    setCAmarillo(pista.avanzar(getCAmarillo(), juego));
                    break;
                case 4:
                    setCVerde(pista.avanzar(getCVerde(), juego));
                    break;
            
        }
    }
}
