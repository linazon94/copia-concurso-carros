/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linaaprende.concursocarros.persistencia;

/**
 *
 * @author linaz
 */

import com.linaaprende.concursocarros.interfaz.Pista;
import com.linaaprende.concursocarros.logica.Carros;
import com.linaaprende.concursocarros.logica.Juego;
import java.sql.*;


public class Conexion {

	private Statement statement;
	private Connection connection;	
	private String jdbc;
	private String ruta;
	private String usuario;
	private String contra;
        //public int idjuego;


	public Conexion(){
		this.connection = null;
		this.statement = null;
		this.jdbc = "com.mysql.jdbc.Driver";
		this.ruta = "jdbc:mysql://localhost/concursocarros";
		this.usuario ="root";
		this.contra ="";		
	}	

	public void cerrarconexion(){
		try	{
			this.connection.close();
		} 
		catch (SQLException e){
			e.printStackTrace();
		}
	}
        
        public String ejecutar(String sentencia){
		try	{
			Class.forName(this.jdbc);
			this.connection = DriverManager.getConnection(this.ruta,this.usuario,this.contra);
			this.statement = this.connection.createStatement();
			this.statement.executeUpdate(sentencia);
			return "Registro Realizado =)";		
		}	 
		catch (SQLException e){
			e.printStackTrace();	
			if (e.getErrorCode()==1062)
				return "El registro ya existe =(";
			else
				return e.toString();
		}
		catch (ClassNotFoundException e){
                    e.printStackTrace();
                    return e.toString();
                }
			
	}
        
            public void guardarendb(Juego juego, Pista pista){
             
             int j;
                for (int i=0;i<4;i++){
                  j = i+1;
                 int idJuego = 1;
                ejecutar("INSERT INTO ganador VALUES('"+idJuego+"','"+juego.getCarrosOrdenados()[i][0]+"','"+j+"')");      
               cambiarIDJuego();
                }
            }
            public void limpiardb(){
                ejecutar("DELETE FROM `concursocarros`.`ganador`"); 
                
            }
            public void cambiarIDJuego(){
                
                ejecutar("UPDATE ganador SET idjuego=1  WHERE 2=2");  
            }
            
}		
 
/*public ResultSet consultar(String sentencia){
		ResultSet resultado=null;
		try	{
			Class.forName(this.jdbc);
			this.connection= DriverManager.getConnection(this.ruta,this.usuario,this.contra);
			this.statement = this.connection.createStatement();
			resultado=statement.executeQuery(sentencia);	
		}	 
		catch (SQLException e){
			e.printStackTrace();
		}
		catch (ClassNotFoundException e){
			e.printStackTrace();
		}				
		return resultado;					
	}*/
