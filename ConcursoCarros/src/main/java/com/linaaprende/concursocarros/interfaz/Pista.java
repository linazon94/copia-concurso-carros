/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linaaprende.concursocarros.interfaz;

import com.linaaprende.concursocarros.logica.Carros;
import com.linaaprende.concursocarros.logica.Juego;

/**
 *
 * @author linaz
 */
public class Pista {
    int avanz;
    int  ValorDado;
    private int mat[][]={{1,0},{2,0},{3,0},{4,0}};
   private int c,f;
    public String[][] primerosLugares;
    public boolean mPodio =false;
   
    public int avanzar(int carro, Juego juego){
        ValorDado = juego.LanzarDado();
        avanz = carro + ValorDado*100;
       return avanz;
    }
    public void listaCompCarros(Carros carros, Juego juego){
        for ( f=0;f<4;f++){
			for( c=0;c<2;c++){
				if(c==1){
                                   switch(mat[f][0]){
                                       case 1 -> mat[f][c] = carros.getCRojo();
                                       case 2 -> mat[f][c] = carros.getCNegro();
                                       case 3 -> mat[f][c] = carros.getCAmarillo();
                                       case 4 -> mat[f][c] = carros.getCVerde();   
                                   } 
                                }
			}
	}
        ordenarCarros(juego);
    }
    
    private void ordenarCarros(Juego juego){
        for(int i = 0; i < 4; i++){
            for(c=0; c<2;c++){
                for(int r = 0; r < 3; r++){
                    if (mat[r][1] < mat[r+1][1]){
                        int tmpc1 = mat[r+1][1];
                        int tmpc0 = mat[r+1][0];
                        mat[r+1][1] = mat[r][1];
                        mat[r+1][0] = mat[r][0];
                        mat[r][1] = tmpc1;
                        mat[r][0] = tmpc0;
                    }
                }
            }
            System.out.println("");
        }
        asignarOrdenCarros(juego);
    }
    
    public void asignarOrdenCarros(Juego juego){
     String Mtemp[][] = {{"",""},{"",""},{"",""},{"",""}};
              for ( f=0;f<4;f++){
			for( c=0;c<2;c++){
                            if(c==0){
                            switch(mat[f][0]){
                                case 1 -> Mtemp[f][c] = "Ironman";
                                case 2 -> Mtemp[f][c] = "Batman";
                                case 3 -> Mtemp[f][c] = "Pikachu";
                                case 4 -> Mtemp[f][c] = "Luigi";
                            }}
                            if(c==1){
                            Mtemp[f][c] = String.valueOf(mat[f][c]);
                            }
			}
		}
            juego.setCarrosOrdenados(Mtemp); 
            cargarPrimerosLugares(juego);
  }
    public String[][] cargarPrimerosLugares(Juego juego){
        System.out.println(mat[2][1]);
        
        if(mat[2][1]>5000){
            primerosLugares=juego.getCarrosOrdenados();
             mPodio=true;
        }
        return primerosLugares;
    }
   
}
